package com.subk.hashmap;

import java.util.Scanner;

public class Hcf {
	public static void main(String args[]) {
		long a, b;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter no1");
		a = s.nextLong();
		System.out.println("Enter no2");
		b = s.nextLong();
		long result = hcfCal(a, b);
		System.out.println("The Result is " + result);
	}

	 static long hcfCal(long a, long b) {
		long temp;
		while (b > 0) {
			temp = b;
			b = a % b;
			a = temp;
		}
		return b;
	}
}