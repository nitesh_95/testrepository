package com.subk.hashmap;

import java.util.Scanner;

public class DisCount {
	public static void main(String args[]) {
		double marketPrice, discount = 1, total;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter marketPrice");
		marketPrice = s.nextDouble();
		if (marketPrice <= 2200) {
			discount = (marketPrice * 20) / 100;
			total = marketPrice - discount;
			System.out.println("The Resulted amount is " + total);
		} else if (marketPrice >= 5000) {
			discount = (marketPrice * 50) / 100;
			total = marketPrice - discount;
			System.out.println(total);
		} else {
			System.out.println("Thanks for shopping no Discount");
		}
	}
}
