package com.subk.hashmap;

import java.util.Scanner;

public class Pattern {
	public static void main(String[] args) {
		double pie = 3.14;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter radius ");
		double radius = s.nextDouble();
		System.out.println("Enter height");
		double height = s.nextDouble();
		double area = 2 * pie * radius * height;
		System.out.println("area is " + area);
	}
}