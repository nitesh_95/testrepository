package com.subk.hashmap;

import java.util.Scanner;


public class Creativity {
	public static void main(String args[]) {
		System.out.println("Enter the terms");
		
		Scanner s = new Scanner(System.in);
		int number = s.nextInt();
		for (int i = 0; i < number; i++)
			System.out.println(fibonnaci2(i));
	}

	public static int fibonnaci(int number) {
		if (number == 1 || number == 2) {
			return 1;

		}
		return fibonnaci(number - 1) + fibonnaci(number - 2);
	}

	public static int fibonnaci2(int number) {
		if (number == 1 || number == 2) {
			return 1;
		}
		int fibo1 = 1, fibo2 = 1, fibonnaci = 2;
		for (int i = 3; i <= number; i++) {
			fibonnaci = fibo1 + fibo2;
			fibo1 = fibo2;
			fibo2 = fibonnaci;
		}
		return fibonnaci;
	}
}