package com.subk.hashmap;

import java.util.Scanner;

public class Average {

	public static void main(String[] args) {
		int rows;
		int columns;
		int i;
		int j;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the rows");
		rows = s.nextInt();
		System.out.println("Enter the columns");
		columns = s.nextInt();
		int mat1[][] = new int[rows][columns];
		int mat2[][] = new int[rows][columns];
		int result[][] = new int[rows][columns];
		System.out.println("Enter the elements of matrix 1");
		for (i = 0; i < rows; i++) {

			for (j = 0; j < columns; j++)
				mat1[i][j] = s.nextInt();

			System.out.println();
		}
		System.out.println("Enter the elements of matrix2");

		for (i = 0; i < rows; i++) {

			for (j = 0; j < columns; j++)
				mat2[i][j] = s.nextInt();

			System.out.println();
		}

		for (i = 0; i < rows; i++)
			for (j = 0; j < columns; j++)
				result[i][j] = mat1[i][j] + mat2[i][j];

		System.out.println("Sum of matrices:-");

		for (i = 0; i < rows; i++) {
			for (j = 0; j < columns; j++)
				System.out.print(result[i][j] + "\t");

			System.out.println();
		}

	}
}
