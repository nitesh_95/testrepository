package com.subk.hashmap;

import java.util.Scanner;

public class Factorial {

	public static void main(String args[]) {
		int fact = 1;
		int number;
		int i;
		System.out.println("Enter the numbers");
		Scanner s = new Scanner(System.in);
		number = s.nextInt();
		for (i = 1; i <= number; i++) {
			fact = fact * i;

		}
		System.out.println(fact);
	}
}