package com.subk;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringController {

	@GetMapping(path = "/welcome")
	public String request() {
		return "welcome to springSecurity";
	}
	@GetMapping(path = "/create")
	public Principal users(Principal principal) {
		return principal;
	}
}
