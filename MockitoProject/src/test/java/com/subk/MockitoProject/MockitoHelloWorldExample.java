package com.subk.MockitoProject;

import static com.subk.MockitoProject.DemoMockitoExample.*;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

import org.junit.Test;

public class MockitoHelloWorldExample {
	@Test
	public void Greets() {
		DemoMockitoExample mockito = mock(DemoMockitoExample.class);
		when(mockito.greet()).thenReturn(HELLO_WORLD);
		System.out.println("Mockito greets: " + mockito.greet());
		assertEquals(mockito.greet(), HELLO_WORLD);
	}
}
