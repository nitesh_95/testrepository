package com.subk.model;

public class UserSignUpRequest {
	
	private String username;
	private String email;
	private String name;
	private String lastname;
	private String phoneNumber;
	private String companyName;
	private String companyPosition;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyPosition() {
		return companyPosition;
	}
	public void setCompanyPosition(String companyPosition) {
		this.companyPosition = companyPosition;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserSignUpRequest [username=" + username + ", email=" + email + ", name=" + name + ", lastname="
				+ lastname + ", phoneNumber=" + phoneNumber + ", companyName=" + companyName + ", companyPosition="
				+ companyPosition + ", password=" + password + "]";
	}

}
