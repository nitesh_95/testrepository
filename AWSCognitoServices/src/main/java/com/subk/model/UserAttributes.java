package com.subk.model;

public class UserAttributes {

	private String name;
	private String phone_number;
	private String surName;
	private String email;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "UserAttributes [name=" + name + ", phone_number=" + phone_number + ", surName=" + surName + ", email="
				+ email + "]";
	}
	
	
}
