package com.subk.model;

import java.util.List;

public class UserDetails {
	
	private String Username;
	private List<UserAttributes> UserAttributes;
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public List<UserAttributes> getUserAttributes() {
		return UserAttributes;
	}
	public void setUserAttributes(List<UserAttributes> userAttributes) {
		UserAttributes = userAttributes;
	}
	@Override
	public String toString() {
		return "UserDetails [Username=" + Username + ", UserAttributes=" + UserAttributes + "]";
	}
	
	
	

}
