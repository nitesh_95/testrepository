package com.subk.aws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:AwsCredentials.properties")
@ConfigurationProperties(prefix = "cognito")
public class CognitoConfig {

	private String clientId;
	private String userPoolId;
	private String endpoint;
	private String region;
	private String identityPoolId;
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getUserPoolId() {
		return userPoolId;
	}
	public void setUserPoolId(String userPoolId) {
		this.userPoolId = userPoolId;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getIdentityPoolId() {
		return identityPoolId;
	}
	public void setIdentityPoolId(String identityPoolId) {
		this.identityPoolId = identityPoolId;
	}
	@Override
	public String toString() {
		return "CognitoConfig [clientId=" + clientId + ", userPoolId=" + userPoolId + ", endpoint=" + endpoint
				+ ", region=" + region + ", identityPoolId=" + identityPoolId + "]";
	}
	
	
	
}
