package com.subk.util;

import org.springframework.stereotype.Component;

@Component
public class AuthenticationResponse {

	private String accessToken;
	private int tokenExpiresin;
	private String status;
	private String reason;
	private String isFirstTimeLogin;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public int getTokenExpiresin() {
		return tokenExpiresin;
	}
	public void setTokenExpiresin(int tokenExpiresin) {
		this.tokenExpiresin = tokenExpiresin;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String getIsFirstTimeLogin() {
		return isFirstTimeLogin;
	}
	public void setIsFirstTimeLogin(String isFirstTimeLogin) {
		this.isFirstTimeLogin = isFirstTimeLogin;
	}
	@Override
	public String toString() {
		return "AuthenticationResponse [accessToken=" + accessToken + ", tokenExpiresin=" + tokenExpiresin + ", status="
				+ status + ", reason=" + reason + ", isFirstTimeLogin=" + isFirstTimeLogin + "]";
	}
	
}
