package com.subk.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpRequest {
	
	public InputStream hitHttpJsonData(String urlString, String jsonDataString) throws Exception {
        System.out.println("url:"+urlString);
        System.out.println("xml:"+jsonDataString);
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("POST");
        conn.setUseCaches(false);
        conn.setRequestProperty("Content-type", "text/xml");
        OutputStream out = conn.getOutputStream();
        out.write(jsonDataString.getBytes());
        //System.out.println("Request Send Success");
        conn.connect();
        InputStream is = conn.getInputStream();
        return is;
    }
}
