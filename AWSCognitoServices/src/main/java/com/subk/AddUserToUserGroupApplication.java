package com.subk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddUserToUserGroupApplication {

	public static void main(String[] args) {
		SpringApplication.run(AddUserToUserGroupApplication.class, args);
	}

}
