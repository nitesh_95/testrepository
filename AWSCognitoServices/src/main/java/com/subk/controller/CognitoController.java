package com.subk.controller;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AuthenticationResultType;
import com.amazonaws.services.cognitoidp.model.GetUserResult;
import com.subk.model.AuthenticationRequest;
import com.subk.model.UserAttributes;
import com.subk.model.UserSignUpRequest;
import com.subk.service.AddUserToUserGroupServivce;
import com.subk.util.AuthenticationResponse;

@Controller
public class CognitoController {

	@Autowired
	AddUserToUserGroupServivce usersignupService;
	@Autowired
	AuthenticationResponse authResp;

	@RequestMapping("/userSignUp")
	public String createUser(@ModelAttribute("userSignUpRequest") UserSignUpRequest request,Model model) {//@RequestBody UserSignUpRequest request
		System.out.println("User SignUp request:::   " + request.toString());
		try {
			System.out.println("Name::::::::::  " + request.getName());
			AdminCreateUserResult authreslt = usersignupService.registerUser(request);
			if (authreslt.getSdkHttpMetadata().getHttpStatusCode() == 200) {
				authResp.setStatus("00");
				authResp.setReason("User Created Successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			authResp.setStatus("S5");
			authResp.setReason("Unable to create User Please try again.....");
			System.out.println("Exception Ocurred:::::::::::::  " + e.getMessage());
		}
		model.addAttribute("authResp", authResp);
		return "login";
	}

	@RequestMapping("/loginUser")
	public String userLogin(@ModelAttribute("authenticationRequest") AuthenticationRequest request,Model model) {
		System.out.println("####################### User Login Start  ####################");
		try {
			System.out.println("User Name:::::::  "+request.getUsername());
//			AdminInitiateAuthResult result =	usersignupService.userLogin("Test123456", "Abhi@2019");
			AdminInitiateAuthResult result = usersignupService.userLogin(request.getUsername(), request.getPassword());
			System.out.println("Login Challenge Name::::::::::::::::::   " + result.getChallengeName());
			System.out.println("Login Response::::::::::::  "+result.toString());
			if (result.getSdkHttpMetadata().getHttpStatusCode() == 200) {
				authResp.setIsFirstTimeLogin(result.getChallengeName());
				if(result.getChallengeName()!=null&&!result.getChallengeName().equals("null")&&authResp.getIsFirstTimeLogin().equals("NEW_PASSWORD_REQUIRED")) {
					authResp.setIsFirstTimeLogin("true");
				}else {
					authResp.setAccessToken(result.getAuthenticationResult().getAccessToken());
					authResp.setTokenExpiresin(result.getAuthenticationResult().getExpiresIn());
				}
				authResp.setStatus("00");
				authResp.setReason("Success");
			} else {
				authResp.setStatus("S5");
				authResp.setReason("Invalid User");
			}
//			
//			System.out.println("Autorization Response::::::::   " + authorizeresult.getUserAttributes());
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("Exception Ocured while Login::::::::::::::  " + e);
		}
		System.out.println("####################### User Login End  ####################");
		model.addAttribute("authResp", authResp);
		return "home";
	}
	
	@RequestMapping("/confirm_changepassword")
	public String changePassword(@ModelAttribute("authenticationRequest") AuthenticationRequest request,Model model) {
		System.out.println("###############  Change Password Start ####################");
		try {
			AuthenticationResultType res = usersignupService.confirmUser(request.getPassword(),
					request.getNewPassword(), request.getUsername());
			System.out.println("Access Token >>>>>>>>>>>>>>>>>" + res.getAccessToken());
			System.out.println("Expire in ::::::: " + res.getExpiresIn());
			if (res.getAccessToken() != null && !res.getAccessToken().equals("")) {
				authResp.setAccessToken(res.getAccessToken());
				authResp.setTokenExpiresin(res.getExpiresIn());
				authResp.setStatus("00");
				authResp.setReason("Please Login with New Credentials");
			} else {
				authResp.setStatus("S5");
				authResp.setReason("Please Enter Valid Credentials");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception Occured In Change Password:::::::::: " + e.getMessage());
		}
		System.out.println("###############  Change Password End ####################");
		model.addAttribute("authResp", authResp);
		return "login";
	}

	@RequestMapping("/getUserDetails")
	public String getUserDetails(@RequestParam("AccessToken") String accessToken,Model model) {
		System.out.println("Get User Details Start......................");
		UserAttributes userAttribute = new UserAttributes();
		try {
			GetUserResult authorizeresult = usersignupService.authorize_getUserDetails(accessToken);
			if(authorizeresult.getSdkHttpMetadata().getHttpStatusCode()==200) {
				authResp.setAccessToken(authorizeresult.getUsername());
				authResp.setStatus("00");
				authResp.setReason("Success");
				String array = authorizeresult.getUserAttributes().toString();
				JSONArray userdetailsArray = new JSONArray(authorizeresult.getUserAttributes().toString());
//				Iterator<Object> userdetailsObj = userdetailsArray.get
				for(int i=0;i<userdetailsArray.length();i++) {
					JSONObject object = userdetailsArray.getJSONObject(i);
					String name = (String)object.getString("Name");
					if(name.equals("name")) {
						userAttribute.setName((String)object.get("Value"));
					}else if(name.equals("phone_number")) {
						userAttribute.setPhone_number(String.valueOf(object.get("Value")));
					}else if(name.equals("family_name")) {
						userAttribute.setSurName((String)object.get("Value"));
					}else if(name.equals("email")) {
						userAttribute.setEmail((String)object.get("Value"));
					}
				}
			}else {
//				authResp.setAccessToken("");
				authResp.setStatus("S5");
				authResp.setReason("User Details not found");
			}
			System.out.println("User Details Result:::::::::::  "+userAttribute);
			
		}catch (Throwable e) {
			// TODO: handle exception
			System.out.println("Exception Occurred: User Details Result  "+e);
		}
		model.addAttribute("authResp", userAttribute);
		System.out.println("Get User Details End......................");
		return "userdetails";
		
	}
	
}
