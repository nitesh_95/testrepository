package com.subk.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AdminRespondToAuthChallengeRequest;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.AuthenticationResultType;
import com.amazonaws.services.cognitoidp.model.ChallengeNameType;
import com.amazonaws.services.cognitoidp.model.DeliveryMediumType;
import com.amazonaws.services.cognitoidp.model.GetUserRequest;
import com.amazonaws.services.cognitoidp.model.GetUserResult;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.subk.aws.config.AwsConfig;
import com.subk.aws.config.CognitoConfig;
import com.subk.model.UserSignUpRequest;
import com.subk.util.HttpRequest;

@Service
public class AddUserToUserGroupServivce {
	@Autowired
	AwsConfig awsIdentityProvider;

	@Autowired
	CognitoConfig cognitoConfig;

//	protected AWSCognitoIdentityProvider cognitoClient;

//	public UserType signUp(UserSignUpRequest signUpRequest) {
//		System.out.println("Sing Up Service Start............." + signUpRequest.getName());
//		System.out.println("User Id::::::::: " + cognitoConfig.getUserPoolId());
//		AWSCognitoIdentityProvider cognitoClient = awsIdentityProvider.getAmazonCognitoIdentityClient();
//		AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
//				.withUserPoolId(cognitoConfig.getUserPoolId()).withUsername("Abhi")
//				.withUserAttributes(new AttributeType().withValue("abhinani492@gmail.com"),
//						new AttributeType().withName("name").withValue("Abhi"),
//						new AttributeType().withName("family_name").withValue("devarakonda"),
//						new AttributeType().withName("phone_number").withValue("9700050157"),
//						new AttributeType().withName("custom:companyName").withValue("BasixSubk"),
//						new AttributeType().withName("custom:companyPosition").withValue("Subk"),
//						new AttributeType().withName("email_verified").withValue("true"))
//				.withTemporaryPassword("TEMPORARY_PASSWORD").withMessageAction("SUPPRESS")
//				.withDesiredDeliveryMediums(DeliveryMediumType.EMAIL).withForceAliasCreation(Boolean.FALSE);
//
//		AdminCreateUserResult createUserResult = cognitoClient.adminCreateUser(cognitoRequest);
//		UserType cognitoUser = createUserResult.getUser();
//		System.out.println("resonse ............." + cognitoUser);
//		return cognitoUser;
//
//	}

	public AdminCreateUserResult registerUser(UserSignUpRequest requestObj) {
		ObjectMapper oMapper = new ObjectMapper();
		
//		Map<String, String> userAttributeKeyValuePairs = oMapper.convertValue(requestObj, Map.class);
		Map<String, String> userAttributeKeyValuePairs = new HashMap<>();
		System.out.println("Map:::::::::::::: "+userAttributeKeyValuePairs);
		userAttributeKeyValuePairs.put("name",requestObj.getUsername());
		userAttributeKeyValuePairs.put("family_name", requestObj.getLastname());
		userAttributeKeyValuePairs.put("phone_number", "+91"+requestObj.getPhoneNumber());
		userAttributeKeyValuePairs.put("email", requestObj.getEmail());
//		userAttributeKeyValuePairs.put("custom:companyName","BasixSubk");
//		userAttributeKeyValuePairs.put("custom:companyPosition","Subk");
		AWSCognitoIdentityProvider cognitoClient = awsIdentityProvider.getAmazonCognitoIdentityClient();
		AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest();
		cognitoRequest.setUsername(requestObj.getUsername());
		cognitoRequest.setUserPoolId(cognitoConfig.getUserPoolId());
		cognitoRequest.setTemporaryPassword(requestObj.getPassword());
		List<AttributeType> userAttributes = new ArrayList<AttributeType>();
		if (userAttributeKeyValuePairs.containsKey("email")) {
			userAttributeKeyValuePairs.put("email_verified", "true");
		}

		for (String key : userAttributeKeyValuePairs.keySet()) {
			AttributeType attributeType = new AttributeType();
			attributeType.setName(key);
			attributeType.setValue(userAttributeKeyValuePairs.get(key));
			userAttributes.add(attributeType);
		}

		cognitoRequest.setUserAttributes(userAttributes);
		cognitoRequest.setForceAliasCreation(false);

		AdminCreateUserResult acr = cognitoClient.adminCreateUser(cognitoRequest);

		System.out.println("Get status Code:::  " + acr.getSdkHttpMetadata().getHttpStatusCode());

		return acr;
	}

	public AuthenticationResultType confirmUser(String tempPassword, String password, String username) {
		System.out.println("Confirm User>>>>>>>>>>>>>>>>>");
		Map<String, String> initialParams = new HashMap<String, String>();
		initialParams.put("USERNAME", username);
		initialParams.put("PASSWORD", tempPassword);
		AdminInitiateAuthRequest initialRequest = new AdminInitiateAuthRequest().withAuthParameters(initialParams)
				.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH).withClientId(cognitoConfig.getClientId())
				.withUserPoolId(cognitoConfig.getUserPoolId());
		AWSCognitoIdentityProvider cognitoClient = awsIdentityProvider.getAmazonCognitoIdentityClient();
		AdminInitiateAuthResult initialResponse = cognitoClient.adminInitiateAuth(initialRequest);
		Map<String, String> challengeResponses = new HashMap<String, String>();
		challengeResponses.put("USERNAME", username);
		challengeResponses.put("PASSWORD", tempPassword);
		challengeResponses.put("NEW_PASSWORD", password);
		AdminRespondToAuthChallengeRequest finalRequest = new AdminRespondToAuthChallengeRequest()
				.withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED).withChallengeResponses(challengeResponses)
				.withClientId(cognitoConfig.getClientId()).withUserPoolId(cognitoConfig.getUserPoolId())
				.withSession(initialResponse.getSession());
		return cognitoClient.adminRespondToAuthChallenge(finalRequest).getAuthenticationResult();

	}

	public AdminInitiateAuthResult userLogin(String username, String password) throws Throwable {
		try {
			Map<String, String> authParams = new HashMap<String, String>();
			authParams.put("USERNAME", username);
			authParams.put("PASSWORD", password);

			AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
					.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH).withAuthParameters(authParams)
					.withClientId(cognitoConfig.getClientId()).withUserPoolId(cognitoConfig.getUserPoolId());
			AWSCognitoIdentityProvider cognitoClient = awsIdentityProvider.getAmazonCognitoIdentityClient();
			AdminInitiateAuthResult authResponse = cognitoClient.adminInitiateAuth(authRequest);
			return authResponse;
		} catch (Throwable e) {
			throw e;
		}

	}

	public GetUserResult authorize_getUserDetails(String accessToken) throws Throwable {
		try {
			AWSCognitoIdentityProvider cognitoClient = awsIdentityProvider.getAmazonCognitoIdentityClient();
			GetUserRequest authRequest = new GetUserRequest().withAccessToken(accessToken);
			
			GetUserResult authResponse = cognitoClient.getUser(authRequest);
			System.out.println("Auth Response::::::::::  "+authResponse);
			return authResponse;
		} catch (Throwable t) {
			throw t;
		}

	}
	

}
