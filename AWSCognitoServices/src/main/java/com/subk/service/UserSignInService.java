package com.subk.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AdminRespondToAuthChallengeRequest;
import com.amazonaws.services.cognitoidp.model.AdminRespondToAuthChallengeResult;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.AuthenticationResultType;
import com.amazonaws.services.cognitoidp.model.ChallengeNameType;
import com.subk.aws.config.AwsConfig;
import com.subk.aws.config.CognitoConfig;
import com.subk.model.AuthenticationRequest;

public class UserSignInService {

	@Autowired
	AwsConfig awsIdentityProvider;

	@Autowired
	CognitoConfig cognitoConfig;

	public AdminInitiateAuthResult signIn(AuthenticationRequest authenticationRequest) {
		AWSCognitoIdentityProvider cognitoClient = awsIdentityProvider.getAmazonCognitoIdentityClient();
		Map<String, String> authParams = new HashMap<>();
		authParams.put("USERNAME", authenticationRequest.getUsername());
		authParams.put("PASS_WORD", authenticationRequest.getPassword());

		AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest();
		authRequest.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH).withClientId(cognitoConfig.getClientId())
				.withUserPoolId(cognitoConfig.getUserPoolId()).withAuthParameters(authParams);

		AdminInitiateAuthResult result = cognitoClient.adminInitiateAuth(authRequest);
System.out.println("Response from Login Success   "+result.toString());
		return result;
	}
}
