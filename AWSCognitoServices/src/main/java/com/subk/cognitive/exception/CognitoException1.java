package com.subk.cognitive.exception;

public enum CognitoException1 {
	CognitoException("requiredNewPassword", "requiredchangePasssword","challengeType");
	String requiredNewPassword;
	String requiredchangePasssword;
	String challengeType;
	
	private CognitoException1(String requiredNewPassword,String requiredchangePasssword, String challengeType) {
		this.requiredNewPassword=requiredNewPassword;
		this.requiredchangePasssword=requiredchangePasssword;
		this.challengeType=challengeType;
	}

	public String getRequiredNewPassword() {
		return requiredNewPassword;
	}

	public void setRequiredNewPassword(String requiredNewPassword) {
		this.requiredNewPassword = requiredNewPassword;
	}

	public String getRequiredchangePasssword() {
		return requiredchangePasssword;
	}

	public void setRequiredchangePasssword(String requiredchangePasssword) {
		this.requiredchangePasssword = requiredchangePasssword;
	}

	public String getChallengeType() {
		return challengeType;
	}

	public void setChallengeType(String challengeType) {
		this.challengeType = challengeType;
	}
	

}
