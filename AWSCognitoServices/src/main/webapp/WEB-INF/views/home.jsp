<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

/* Set a style for all buttons */
button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
  position: relative;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.container {
  padding: 16px;
}

span.psw {
  float: right;

}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body onload="checkUser();">
<div id="changepswdreqired" class="hide">
  
  <form class="" action="${pageContext.request.contextPath}/confirm_changepassword" method="post">
    

    <div class="container" style="width: 400px;">
       <label for="uname"><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="username" required>
      <label for="psw"><b>Old Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" required>
      <label for="psw"><b>New Password</b></label>
      <input type="text" placeholder="Enter Password" name="newPassword" required>

      <label for="psw-confirm"><b>Confirm Password</b></label>
      <input type="password" placeholder="Confirm Password" name="psw-confirm" required>
        
      <button type="submit">Submit</button>
      <%-- <span class="sign-up"><a href="${pageContext.request.contextPath}/signup">SignUp</a></span>
      <span class="psw">Forgot <a href="#">password?</a></span> --%>
    </div>
    
  </form>
</div>
<div id="home"  class="container" style="width: 400px;">
<h2>Welcome ............................Login Successfully.....</h2>

<form id="" action="${pageContext.request.contextPath}/getUserDetails">
<input type="text" class="hide" id="acceeToken" name="AccessToken" value="${authResp.accessToken}"/>
<input type="text" class="hide" id="expiresIn" name="expiresIn" value="${authResp.tokenExpiresin}"/>
<button type="submit">GetUserDetails</button>
</form>
</div>
<script>
function checkUser(){
	var status = '${authResp.isFirstTimeLogin}';
	/* var response = '${authResp.reason}'; */
	if(status!=""){
		$("#changepswdreqired").removeClass("hide");
		$("#home").addClass("hide");
	}
}
</script>
</body>
</html>