package com.subk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdpServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdpServiceApplication.class, args);
	}

}
