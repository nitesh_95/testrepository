package com.subk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HoneywellServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HoneywellServicesApplication.class, args);
	}

}
