package com.subk;

import java.util.HashMap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class HoneywellService {

	@GetMapping("/honeywell/{eno}/{name}/{salary}/{address}")
	public EmployeeDetails getEmployment(@PathVariable Integer eno, @PathVariable("name") String name,
			@PathVariable Integer salary, @PathVariable String address) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("eno", eno);
		hashMap.put("salary", salary);
		RestTemplate rt = new RestTemplate();
		ResponseEntity<EmployeeDetails> responseEntity = rt
				.getForEntity("http://localhost:6001/adp-tax-cal/{eno}/{salary}", EmployeeDetails.class, hashMap);
		EmployeeDetails employeeDetails = responseEntity.getBody();
		employeeDetails.setName(name);
		employeeDetails.setAddress(address);
		return employeeDetails;
	}
}
