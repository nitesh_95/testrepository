package com.subk;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "adp-tax-cal", url = "http://localhost:6001")

public interface AdpTaxCalProxy {

	@GetMapping(path = "/adp-tax-cal/{eno}/{salary}")
	public EmployeeDetails calTax(@PathVariable("eno") Integer eno, @PathVariable("salary") Integer salary);
}
