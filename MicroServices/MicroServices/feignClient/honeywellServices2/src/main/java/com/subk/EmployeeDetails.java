package com.subk;

public class EmployeeDetails {

	private Integer eno;
	private String name;
	private Integer salary;
	private Integer tax;
	private Integer netPay;
	private String address;
	private Integer portNo;

	public EmployeeDetails() {

	}

	public EmployeeDetails(Integer eno, String name, Integer salary, Integer tax, Integer netPay, String address,
			Integer portNo) {

		this.eno = eno;
		this.name = name;
		this.salary = salary;
		this.tax = tax;
		this.netPay = netPay;
		this.address = address;
		this.portNo = portNo;
	}

	public Integer getEno() {
		return eno;
	}

	public void setEno(Integer eno) {
		this.eno = eno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Integer getTax() {
		return tax;
	}

	public void setTax(Integer tax) {
		this.tax = tax;
	}

	public Integer getNetPay() {
		return netPay;
	}

	public void setNetPay(Integer netPay) {
		this.netPay = netPay;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPortNo() {
		return portNo;
	}

	public void setPortNo(Integer portNo) {
		this.portNo = portNo;
	}

}
