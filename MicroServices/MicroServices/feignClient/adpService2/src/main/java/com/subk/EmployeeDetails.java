package com.subk;

public class EmployeeDetails {
 
	private Integer eno;
	private Integer salary;
	private Integer netPay;
	private Integer tax;
	private Integer portNo;

	
	public Integer getPortNo() {
		return portNo;
	}
	public void setPortNo(Integer portNo) {
		this.portNo = portNo;
	}
	public EmployeeDetails() {
	
	}
	public Integer getEno() {
		return eno;
	}
	public void setEno(Integer eno) {
		this.eno = eno;
	}
	public Integer getSalary() {
		return salary;
	}
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	public Integer getNetPay() {
		return netPay;
	}
	public void setNetPay(Integer netPay) {
		this.netPay = netPay;
	}
	public Integer getTax() {
		return tax;
	}
	public void setTax(Integer tax) {
		this.tax = tax;
	}
	
	public EmployeeDetails(Integer eno, Integer salary, Integer netPay, Integer tax, Integer portNo) {
		this.eno = eno;
		this.salary = salary;
		this.netPay = netPay;
		this.tax = tax;
	}
	
}
