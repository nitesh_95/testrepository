package com.subk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HoneywellService {

	@Autowired
	private AdpTaxCalProxy adpCall;

	@GetMapping("/honeywell/{eno}/{name}/{salary}/{address}")
	public EmployeeDetails getEmployment(@PathVariable Integer eno, @PathVariable("name") String name,
			@PathVariable Integer salary, @PathVariable String address) {
		EmployeeDetails employeeDetails = adpCall.calTax(eno, salary);
		employeeDetails.setName(name);
		employeeDetails.setAddress(address);
		return employeeDetails;
	}
}
