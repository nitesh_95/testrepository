package com.subk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = "com.subk")
public class HoneywellServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HoneywellServicesApplication.class, args);
	}

}
