package com.subk;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "adp-tax-cal")
@RibbonClient(name = "adp-tax-cal")
public interface AdpTaxCalProxy {

	@GetMapping(path = "/adp-tax-cal/{eno}/{salary}")
	public EmployeeDetails calTax(@PathVariable("eno") Integer eno, @PathVariable("salary") Integer salary);
}
