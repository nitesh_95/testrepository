package com.subk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdpSercvice {
	@Autowired
	private Environment environment;

	@GetMapping(path = "/adp-tax-cal/{eno}/{salary}")
	public EmployeeDetails calTax(@PathVariable("eno") Integer eno, @PathVariable("salary") Integer salary) {
		Integer tax = (salary * 30) / 100;
		Integer netPay = salary - tax;
		EmployeeDetails employeeDetails = new EmployeeDetails();
		employeeDetails.setEno(eno);
		employeeDetails.setSalary(salary);
		employeeDetails.setNetPay(netPay);
		employeeDetails.setTax(tax);
		employeeDetails.setPortNo(Integer.parseInt(environment.getProperty("local.server.port")));
		return employeeDetails;
	}
}
