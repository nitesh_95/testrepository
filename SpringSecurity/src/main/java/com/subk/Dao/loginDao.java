package com.subk.Dao;

import com.subk.model.User;

public interface loginDao {

	public int register (User user);
	public User getUserProfile(String username);
}
