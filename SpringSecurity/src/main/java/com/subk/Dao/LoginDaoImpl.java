package com.subk.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.subk.model.User;

@Component
public class LoginDaoImpl implements loginDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override

	public int register(User user) {
		String query = " insert into user_profile(username, password, salt) values ( ?, ?, ?)";
		int count = jdbcTemplate.update(query, new Object[] { user.getUsername(), user.getPassword(), user.getSalt() });
		return count;
	}

	@Override
	public User getUserProfile(String username) {
		String query = "select * from  user_profile where username=?";
		User user = jdbcTemplate.queryForObject(query, new Object[] { username }, User.class);
		return user;
	}

}
