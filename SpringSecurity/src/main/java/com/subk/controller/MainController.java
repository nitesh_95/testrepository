package com.subk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.subk.model.User;
import com.subk.services.LoginService;
import com.subk.util.RandomString;

@Controller
public class MainController {

	@Autowired
	private ShaPasswordEncoder shaPasswordEncoder;

	@Autowired
	private RandomString randomString;

	@Autowired
	private LoginService loginService;

	@RequestMapping("/")
	public String index() {
		return "index";
	}

	@RequestMapping("/registerForm")
	public ModelAndView registerForm(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return new ModelAndView("register");
	}

	@RequestMapping("/login")
	public ModelAndView loginForm(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)

	public ModelAndView register(@RequestParam("username") String username, @RequestParam("password") String password) {

		User user = new User();
		String salt = randomString.getSalt(10);

		String encodePassword = shaPasswordEncoder.encodePassword(password, salt);
		user.setSalt(salt);
		user.setPassword(encodePassword);
		user.setUsername(username);
		int count = loginService.register(user);
		if (count > 0)
			System.out.println("user registered successfully...");
		else {
			System.out.println("user not registered...");
		}
		return new ModelAndView("register");

	}

}