package com.subk.util;

import org.springframework.context.annotation.Configuration;

@Configuration
public class RandomString {

	public String getSalt(int length) {
		StringBuffer buffer = new StringBuffer();
		int minimum = 33, maximum = 120, randomNum;
		for (int i = 0; i < length; i++) {
			randomNum = (int) (Math.random() * maximum);
			if (randomNum < minimum) {
				randomNum += minimum;
			}
			buffer.append((char) randomNum);
		}
		return buffer.toString();
	}
}
