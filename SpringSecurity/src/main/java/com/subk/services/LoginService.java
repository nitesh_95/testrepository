package com.subk.services;

import com.subk.model.User;

public interface LoginService {
	public int register(User user);

	public User getUserProfile(String username);
}
