package com.subk.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subk.model.User;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private com.subk.Dao.loginDao loginDao;

	@Override
	public int register(User user) {
		return loginDao.register(user);
	}

	@Override
	public User getUserProfile(String username) {
		return loginDao.getUserProfile(username);
	}

}
