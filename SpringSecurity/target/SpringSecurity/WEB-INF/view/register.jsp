<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Fill up the form to Register</h2>

	<form:form action="./register" method="POST" modelAttribute="user">
		<table style="with: 50%">
			<tr>
				<td>UserName</td>
				<td><form:input path="username" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><form:input path="password" /></td>
			</tr>
		</table>
		<input type="submit" value="Submit" />
	</form:form>

</body>
</html>