<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>Login</title>
</head>
<body>
	<div id="login-box">
		<h1>Log in using your credentials!</h1>

		<form name="loginForm" action=""
			${pageContext.request.contextPath}/login>

				<label>Username: </label> <input type="text" id="username" />

				<label>Password: </label> <input type="password" id="password" />

			<input type="submit" value="Login" class="btn btn-default" /> <input
				type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

		</form>
	</div>
</body>
</html>
